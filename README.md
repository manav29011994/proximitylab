using MVVM Architecture from jetpack 
using Single activity and multiple fragment Architecture
using Navigation component for navigate between fragemnt and using navigation args for send data from one fragment to other
App has Single Activty i.e HomeActivity and Home Fragment is the first fragment that gets loaded by default on launch of activity 
using WebSocketClient for subscribing to the ws feed
App is using map to store the all the cities data in Viewmodel then we are using live data to observe data in  fragments, Note that i have used single SharedViewModel because there is no use case for using Viewmodel for each fragment as all the logic related to data fetch is done from activity only.
using MPAndroidChart library for displaying the cart in on click of any card in home page

Total time spent is 5 hrs
Setting navigation(Navigation component) and Ui component(Recycleview, Adapter, Fragment)- 2 hour
WebSocket connection- 30 mins
searching the library and integrating in the detail page- 1 hour
Writting the Readme file and setting up the repo and sharing the project- 30 mins


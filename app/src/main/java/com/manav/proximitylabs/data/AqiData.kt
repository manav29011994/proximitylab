package com.manav.proximitylabs.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AqiData(
    val city: String?,
    val aqi: Float,
    var lastUpdate:Long
):Parcelable

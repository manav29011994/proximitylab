package com.manav.proximitylabs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.manav.proximitylabs.data.AqiData

class HomeViewModel : ViewModel() {

    private val aqiListMld = MutableLiveData<List<AqiData>>()
    val map = mutableMapOf<String, AqiData>()

    fun getAqiListLiveData(): LiveData<List<AqiData>> = aqiListMld

    fun setData(list: List<AqiData>) {
        for(items in list)
            items.city?.let { map.put(it,items) }

        aqiListMld.postValue(list)
    }

}
package com.manav.proximitylabs.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.gson.GsonBuilder
import com.manav.proximitylabs.R
import com.manav.proximitylabs.data.AqiData
import com.manav.proximitylabs.viewmodel.HomeViewModel
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI
import javax.net.ssl.SSLSocketFactory

class HomeActivity : AppCompatActivity() {
    private lateinit var webSocketClient: WebSocketClient


    private val homeViewModel by lazy {
       ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

    }


    override fun onResume() {
        super.onResume()
        initWebSocket()
    }

    override fun onPause() {
        super.onPause()
        webSocketClient.close()
    }

    private fun initWebSocket() {
        val aqiIndexUri: URI? = URI(WEB_SOCKET_URL)

        createWebSocketClient(aqiIndexUri)

        val socketFactory: SSLSocketFactory = SSLSocketFactory.getDefault() as SSLSocketFactory
        webSocketClient.setSocketFactory(socketFactory)
        webSocketClient.connect()
    }

    private fun createWebSocketClient(uri: URI?) {
        webSocketClient = object : WebSocketClient(uri) {

            override fun onOpen(handshakedata: ServerHandshake?) {
                Log.d(TAG, "onOpen")
            }

            override fun onMessage(message: String?) {
                //  Log.d(TAG, "onMessage: $message")
                setUpAqiList(message)
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {
                Log.d(TAG, "onClose")
            }

            override fun onError(ex: Exception?) {
                Log.e(TAG, "onError: ${ex?.message}")
            }

        }
    }

    private fun setUpAqiList(message: String?) {
        message?.let {
            val apiDataList= GsonBuilder().create().fromJson(message,Array<AqiData>::class.java).toList()
            homeViewModel.setData(apiDataList)
        }
    }


    companion object {
        const val WEB_SOCKET_URL = "wss://city-ws.herokuapp.com/"
        const val TAG = "ProximityLabs"
    }
}
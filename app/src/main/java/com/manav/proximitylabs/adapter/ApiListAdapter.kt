package com.manav.proximitylabs.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.manav.proximitylabs.R
import com.manav.proximitylabs.data.AqiData
import com.manav.proximitylabs.fragement.ClickListener

class ApiListAdapter(private val onClickListener:ClickListener,private val aqiDataList: MutableList<AqiData>?): RecyclerView.Adapter<AqiViewHolder>() {

    private val map = mutableMapOf<String, AqiData>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AqiViewHolder {
        return AqiViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.rv_card_item, parent, false),
            onClickListener
        )
    }

    override fun onBindViewHolder(holder: AqiViewHolder, position: Int) {
        val aqiData = aqiDataList?.get(position)
        holder.setData(aqiData, position)
    }

    override fun getItemCount(): Int {
        return aqiDataList?.size?:0
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setLatestData(newAqiData: List<AqiData>) {
        val finalData=processDataForDuplicates(newAqiData)
        aqiDataList?.clear()
        aqiDataList?.addAll(finalData)
        notifyDataSetChanged()
    }


    private fun processDataForDuplicates(list: List<AqiData>): List<AqiData> {
        for(item in list){
            item.lastUpdate=System.currentTimeMillis()
            map[item.city!!] = item
        }

        val tempList = ArrayList(map.values)

        return tempList.sortedWith(compareBy { it.city })
    }

}



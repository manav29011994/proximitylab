package com.manav.proximitylabs.adapter

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.manav.proximitylabs.R
import com.manav.proximitylabs.data.AqiData
import com.manav.proximitylabs.fragement.ClickListener


class AqiViewHolder(itemView: View, private val onClickListener: ClickListener) :RecyclerView.ViewHolder(itemView) {

    var tvCity:TextView = itemView.findViewById(R.id.cityName)
    var tvAqi:TextView=itemView.findViewById(R.id.currentAQI)
    var tvLastUpdated:TextView=itemView.findViewById(R.id.lastUpdated)
    fun setData(aqiData: AqiData?,position: Int){
      if(aqiData==null)
          return

        aqiData.aqi.let {
            tvAqi.text= it.roundTo(2).toString()
            tvAqi.setTextColor(ContextCompat.getColor(itemView.context, getColorBasedOnAqi(it)))
        }

        aqiData.city.let {
            tvCity.text=it
        }

        tvLastUpdated.text= lastUpdatedTime(aqiData.lastUpdate)

        itemView.setOnClickListener{
            onClickListener.onClick(aqiData)
        }

    }

}

fun Float.roundTo(n : Int) : Float {
    return "%.${n}f".format(this).toFloat()
}

fun getColorBasedOnAqi(aqiValue:Float):Int{
   return when(aqiValue){

       in 0f..50f-> return R.color.color_good
       in 50f..100f->return R.color.color_satisfactory
       in 100f..200f->return R.color.color_moderate
       in 200f..300f->return R.color.color_poor
       in 300f..400f->return R.color.color_very_poor
       in 400F..500f->return R.color.color_severe
       else -> R.color.accent_color_29
   }
}


fun lastUpdatedTime(lastTs:Long) :String{
    val milliseconds=System.currentTimeMillis()-lastTs
    val seconds = (milliseconds / 1000).toInt() % 60
    val minutes = (milliseconds / (1000 * 60) % 60)
    val hours = (milliseconds / (1000 * 60 * 60) % 24)

    if(hours>0){
        return hours.toString().plus(" hrs ").plus(minutes.toString()).plus(" minutes ago")
    }
    if(minutes>0){
        return (minutes.toString()).plus(" minutes ago")
    }
    if(seconds in 5..59){
        return "$seconds seconds ago"
    }
    return "few seconds ago"
}

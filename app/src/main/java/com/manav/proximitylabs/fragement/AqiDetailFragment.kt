package com.manav.proximitylabs.fragement

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import com.manav.proximitylabs.R
import com.manav.proximitylabs.data.AqiData
import com.manav.proximitylabs.viewmodel.HomeViewModel

class AqiDetailFragment : Fragment(R.layout.fragment_detail_page) {


    val args:AqiDetailFragmentArgs by navArgs()

    private var mChart: LineChart? = null

    private val MAX_AQI = 400.0f
    private val LIMIT_MAX_AQI = 400.0f

    lateinit var handler: Handler


    private val homeViewModel by lazy {
        activity?.let { ViewModelProvider(it).get(HomeViewModel::class.java) }
    }

    private val runnable =object:Runnable{
        override fun run() {
            val latestData=args.aqiData.city?.let { homeViewModel?.map?.get(it) }
            if (latestData != null) {
                addEntry(latestData)
            }
            handler.postDelayed({ this.run() }, 30000)
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mChart=view.findViewById(R.id.chart) as LineChart
        setupChart()
        setupAxes()
        setupData()
        setLegend()
        handler = Handler(Looper.getMainLooper())
    }



    private fun setupChart() {
        // disable description text
        mChart?.description?.isEnabled = false
        // enable touch gestures
        mChart?.setTouchEnabled(true)
        // if disabled, scaling can be done on x- and y-axis separately
        mChart?.setPinchZoom(true)
        // enable scaling
        mChart?.setScaleEnabled(true)
        mChart?.setDrawGridBackground(false)
        // set an alternative background color
        mChart?.setBackgroundColor(Color.DKGRAY)
    }

    private fun setupAxes() {
        val xl = mChart?.xAxis
        xl?.textColor = Color.WHITE
        xl?.setDrawGridLines(false)
        xl?.setAvoidFirstLastClipping(true)
        xl?.isEnabled = true
        val leftAxis = mChart?.axisLeft
        leftAxis?.textColor = Color.WHITE
        leftAxis?.axisMaximum = MAX_AQI
        leftAxis?.axisMinimum = 0f
        leftAxis?.setDrawGridLines(true)
        leftAxis?.typeface = Typeface.DEFAULT_BOLD
        val rightAxis = mChart?.axisRight
        rightAxis?.isEnabled = false

        // Add a limit line
        val ll = LimitLine(LIMIT_MAX_AQI, "Upper Limit")
        ll.lineWidth = 2f
        ll.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        ll.textSize = 10f
        ll.textColor = Color.WHITE
        ll.lineColor = Color.RED
        // reset all limit lines to avoid overlapping lines
        leftAxis?.removeAllLimitLines()
        leftAxis?.addLimitLine(ll)
        // limit lines are drawn behind data (and not on top)
        leftAxis?.setDrawLimitLinesBehindData(true)
    }

    private fun setupData() {
        val data = LineData()
        data.setValueTextColor(Color.WHITE)

        // add empty data
        mChart?.data = data
    }

    private fun setLegend() {
        // get the legend (only possible after setting data)
        val l = mChart?.legend

        // modify the legend ...
        l?.form = Legend.LegendForm.CIRCLE
        l?.textColor = Color.WHITE
    }

    private fun createSet(): LineDataSet {
        val set = LineDataSet(null, "Air Quality Index (AQI)")
        set.axisDependency = YAxis.AxisDependency.LEFT
        set.setColors(ColorTemplate.MATERIAL_COLORS[0])
        set.setCircleColor(Color.WHITE)
        set.lineWidth = 2f
        set.circleRadius = 4f
        set.valueTextColor = Color.WHITE
        set.valueTextSize = 10f
        // To show values of each point
        set.setDrawValues(true)
        return set
    }

    private fun addEntry(stat: AqiData) {
        val data = mChart!!.data
        if (data != null) {
            var set = data.getDataSetByIndex(0)
            if (set == null) {
                set = createSet()
                data.addDataSet(set)
            }
            data.addEntry(Entry(set.entryCount.toFloat(), stat.aqi), 0)

            // let the chart know it's data has changed
            data.notifyDataChanged()
            mChart?.notifyDataSetChanged()

            // limit the number of visible entries
            mChart?.setVisibleXRangeMaximum(6f)

            // move to the latest entry
            mChart?.moveViewToX(data.entryCount.toFloat())
        }
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

    override fun onResume() {
        super.onResume()
        val latestData=args.aqiData.city?.let { homeViewModel?.map?.get(it) }
        if (latestData != null) {
            addEntry(latestData)
        }
        handler.postDelayed(runnable, 30000)
    }

}


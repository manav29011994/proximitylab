package com.manav.proximitylabs.fragement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.manav.proximitylabs.R
import com.manav.proximitylabs.adapter.ApiListAdapter
import com.manav.proximitylabs.data.AqiData
import com.manav.proximitylabs.viewmodel.HomeViewModel

class HomeFragment : Fragment(),ClickListener {

    private lateinit var listAdapter: ApiListAdapter
    private lateinit var rvAqiData: RecyclerView
    private var aqiListData = mutableListOf<AqiData>()


    private val homeViewModel by lazy {
        activity?.let { ViewModelProvider(it).get(HomeViewModel::class.java) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rvAqiData = view.findViewById(R.id.rvAqi)

        rvAqiData.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        listAdapter = ApiListAdapter(this,aqiListData)
        rvAqiData.adapter = listAdapter

        initObserver()

    }

    private fun initObserver() {
        homeViewModel?.getAqiListLiveData()?.observe(viewLifecycleOwner, {

            listAdapter.setLatestData(it)
        })
    }

    override fun onClick(data: AqiData) {
        Toast.makeText(activity,data.toString(),Toast.LENGTH_SHORT).show()

        val action=HomeFragmentDirections.actionHomeFragmentToAqiDetailFragment2(data)
        findNavController().navigate(action)
    }


}

interface ClickListener{
    fun onClick(data: AqiData)
}